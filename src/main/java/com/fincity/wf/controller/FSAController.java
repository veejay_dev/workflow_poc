package com.fincity.wf.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fincity.wf.exceptions.ResourceNotFoundException;
import com.fincity.wf.model.WorkflowFSA;
import com.fincity.wf.repo.FSAByWorkflow;
import com.fincity.wf.repo.WorkflowFSARepository;

@RestController
public class FSAController {
	@Autowired
	private WorkflowFSARepository fsaRepo;
	
	@GetMapping("/fsa")
	public Page<WorkflowFSA> getFSA(@RequestParam(value = "wfId") Long wfId, Pageable pageable) {
		Specification<WorkflowFSA> spec = new FSAByWorkflow(wfId);
		return fsaRepo.findAll(spec, pageable);
    }
	
	@PostMapping("/fsa")
    public WorkflowFSA createFSA(@Valid @RequestBody WorkflowFSA fsa) {
        return fsaRepo.save(fsa);
    }
	
	@DeleteMapping("/fsa/{fsaID}")
    public ResponseEntity<?> deleteControl(@PathVariable Long ctrlID) {
        return fsaRepo.findById(ctrlID)
                .map(ctrl -> {
                	fsaRepo.delete(ctrl);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException(
                		"FSA Step not found with id " + ctrlID));
    }
}
