package com.fincity.wf.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fincity.wf.exceptions.ResourceNotFoundException;
import com.fincity.wf.model.Control;
import com.fincity.wf.repo.ControlRepository;

@RestController
public class CtrlController {
	@Autowired
	private ControlRepository ctrlRepo;
	
	@GetMapping("/controls")
	public Page<Control> getControls(Pageable pageable) {
        return ctrlRepo.findAll(pageable);
    }
	
	@PostMapping("/controls")
    public Control createControls(@Valid @RequestBody Control control) {
        return ctrlRepo.save(control);
    }
	
	@DeleteMapping("/controls/{ctrlID}")
    public ResponseEntity<?> deleteControl(@PathVariable Long ctrlID) {
        return ctrlRepo.findById(ctrlID)
                .map(ctrl -> {
                	ctrlRepo.delete(ctrl);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException(
                		"Control not found with id " + ctrlID));
    }

}
