package com.fincity.wf.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fincity.wf.exceptions.ResourceNotFoundException;
import com.fincity.wf.model.Workflow;
import com.fincity.wf.repo.WorkflowRepository;

@RestController
public class WorkflowController {
	
	@Autowired
	private WorkflowRepository wfRepo;
	
	@GetMapping("/workflows")
	public Page<Workflow> getQuestions(Pageable pageable) {
        return wfRepo.findAll(pageable);
    }
	
	@PostMapping("/workflows")
    public Workflow createQuestion(@Valid @RequestBody Workflow workflow) {
        return wfRepo.save(workflow);
    }
	
	@DeleteMapping("/workflows/{wfID}")
    public ResponseEntity<?> deleteWorkflow(@PathVariable Long wfID) {
        return wfRepo.findById(wfID)
                .map(workflow -> {
                    wfRepo.delete(workflow);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Workflow not found with id " + wfID));
    }
}
