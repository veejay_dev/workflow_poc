package com.fincity.wf.repo;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.fincity.wf.model.WorkflowFSA;

@Repository
public interface WorkflowFSARepository  extends JpaRepository<WorkflowFSA, Long>, JpaSpecificationExecutor<WorkflowFSA> {

}
