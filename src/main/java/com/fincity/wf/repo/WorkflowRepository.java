package com.fincity.wf.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fincity.wf.model.Workflow;

@Repository
public interface WorkflowRepository extends JpaRepository<Workflow, Long> {

}
