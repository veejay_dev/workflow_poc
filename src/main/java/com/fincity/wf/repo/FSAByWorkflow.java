package com.fincity.wf.repo;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.fincity.wf.model.WorkflowFSA;

public class FSAByWorkflow implements Specification<WorkflowFSA> {
	private Long workflowId;

	public FSAByWorkflow(Long workflowId) {
		super();
		this.workflowId = workflowId;
	}

	@Override
	public Predicate toPredicate(Root<WorkflowFSA> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
		return builder.equal(root.get("workflow"), this.workflowId);
	}
}