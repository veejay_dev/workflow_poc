package com.fincity.wf.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fincity.wf.model.Control;

@Repository
public interface ControlRepository extends JpaRepository<Control, Long> {

}
