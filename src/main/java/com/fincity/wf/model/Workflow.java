package com.fincity.wf.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "workflows")
public class Workflow extends BPMModel {
	@Id
    @GeneratedValue(generator = "wf_generator")
    @SequenceGenerator(
            name = "wf_generator",
            sequenceName = "wf_sequence",
            initialValue = 1000
    )
    private Long id;
	
	@NotBlank
    @Size(min = 3, max = 100)
	private String name;
	
	private String description;
	
	private Boolean isActive;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
}
