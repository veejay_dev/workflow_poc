package com.fincity.wf.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "workflow_fsa")
public class WorkflowFSA {
	@Id
    @GeneratedValue(generator = "fsa_generator")
    @SequenceGenerator(
            name = "fsa_generator",
            sequenceName = "fsa_sequence",
            initialValue = 1
    )
    private Long id;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "workflow_id", referencedColumnName = "id")
	private Workflow workflow;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "source_ctrl_id", referencedColumnName = "id")
	private Control sourceCtrl;
	
	private String outputState;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "target_ctrl_id", referencedColumnName = "id")
	private Control targetCtrl;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Workflow getWorkflow() {
		return workflow;
	}

	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}

	public Control getSourceCtrl() {
		return sourceCtrl;
	}

	public void setSourceCtrl(Control sourceCtrl) {
		this.sourceCtrl = sourceCtrl;
	}

	public String getOutputState() {
		return outputState;
	}

	public void setOutputState(String outputState) {
		this.outputState = outputState;
	}

	public Control getTargetCtrl() {
		return targetCtrl;
	}

	public void setTargetCtrl(Control targetCtrl) {
		this.targetCtrl = targetCtrl;
	}
}
