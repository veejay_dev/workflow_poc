package com.fincity.wf.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;

@Entity
@Table(name = "controls")
@TypeDef(
	    name = "jsonb",
	    typeClass = JsonBinaryType.class
	)
public class Control extends BPMModel {
	@Id
    @GeneratedValue(generator = "ctrl_generator")
    @SequenceGenerator(
            name = "ctrl_generator",
            sequenceName = "ctrl_sequence",
            initialValue = 1
    )
    private Long id;
	
	@NotBlank
    @Size(min = 3, max = 100)
	private String name;
	
	private String description;
	
	@Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
	private Implementation impl;
	
	@Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
	private InputParams inputParams;
	
	@Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
	private OutputParams outputParams;

	public InputParams getInputParams() {
		return inputParams;
	}

	public void setInputParams(InputParams inputParams) {
		this.inputParams = inputParams;
	}

	public OutputParams getOutputParams() {
		return outputParams;
	}

	public void setOutputParams(OutputParams outputParams) {
		this.outputParams = outputParams;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Implementation getImpl() {
		return impl;
	}

	public void setImpl(Implementation impl) {
		this.impl = impl;
	}
	
}

class Implementation {
	private String type;
	private String method;
	private String componentId;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getComponentId() {
		return componentId;
	}
	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}
}

class Parameter {
	private String name;
	private String type;
	private String category;
	private boolean mandatory;
	private String value;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public boolean isMandatory() {
		return mandatory;
	}
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}

class InputParams {
	private List<Parameter> params;

	public List<Parameter> getParams() {
		return params;
	}

	public void setParams(List<Parameter> params) {
		this.params = params;
	}
}

class OutputParams {
	private List<Parameter> params;

	public List<Parameter> getParams() {
		return params;
	}

	public void setParams(List<Parameter> params) {
		this.params = params;
	}
}
