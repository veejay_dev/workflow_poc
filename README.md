# Workflows (aka BPM) PoC

This is a PoC implementation for Workflows (aka BPM) project. 

 - Please do not expect Production ready features - Logging, Error Handling, Cached APIs, etc
 - Please note that Business Services were ommitted for now, and Controller directly makes Repository calls. Libraries like Lombok can further simplify the code
 - API routes are not versioned and very raw.


Having said that, create a DB called `bpm` in Postgres and run the code. The Schema should be created automatically.

```sh
bpm=# \d
             List of relations
 Schema |     Name      |   Type   | Owner
--------+---------------+----------+-------
 public | controls      | table    | cepl
 public | ctrl_sequence | sequence | cepl
 public | fsa_sequence  | sequence | cepl
 public | wf_sequence   | sequence | cepl
 public | workflow_fsa  | table    | cepl
 public | workflows     | table    | cepl
(6 rows)
```

Will try to create one end to end flow, like E-KYC and publish the Postman API collection soon.
